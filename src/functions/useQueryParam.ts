import { computed } from "vue"
import { LocationQuery } from "vue-router"

import router from "src/router"

type QueryParam = Extract<LocationQuery[string], any>

export type Parser<T> = (value: QueryParam | undefined) => [boolean, T]
export type Stringifier<T> = (value: T | undefined) => QueryParam | undefined
export type QueryParamOptions<T> = {
	parse: Parser<T>
	stringify: Stringifier<T>
}

const useQueryParam = <T>(name: string, { parse, stringify }: QueryParamOptions<T>) => {
	const parameterValue = computed(() => router.currentRoute.value.query[name])
	const isDefined = computed(() => parameterValue.value !== undefined)

	const parseResult = computed(() => parse(parameterValue.value))
	const isValid = computed(() => parseResult.value[0])
	const parsedValue = computed(() => parseResult.value[1])

	const value = computed({
		get: () => parsedValue.value,
		set: (value) => {
			router.replace({
				...router.currentRoute.value,
				query: {
					...router.currentRoute.value.query,
					[name]: stringify(value),
				},
			})
		},
	})

	return {
		value,
		isDefined,
		isValid,
	}
}

export default useQueryParam

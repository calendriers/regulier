const isLeapYear = (year: number): boolean => {
	if (year < 0) {
		year += 1
	}

	return year % 4 === 0 && (year % 100 !== 0 || (year % 400 === 0 && year % 4_000 !== 0))
}

export default isLeapYear

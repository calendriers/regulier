export const DAYS_PER_WEEK = 7
export const DAYS_PER_YEAR = 365
export const DAYS_PER_LEAP_YEAR = 366

export const WEEK_DAYS = [
	"lundi",
	"mardi",
	"mercredi",
	"jeudi",
	"vendredi",
	"samedi",
	"dimanche",
] as const

export const MONTHS = [
	"janvier",
	"février",
	"mars",
	"avril",
	"mai",
	"juin",
	"juillet",
	"aout",
	"septembre",
	"octobre",
	"novembre",
	"décembre",
] as const

export const HOLIDAYS = [
	{ month: 4, day: 7, name: "Pâques" },
	{ month: 4, day: 8, name: "Lundi de Pâques" },
	{ month: 5, day: 1, name: "Fête du Travail" },
	{ month: 5, day: 8, name: "Capitulation" },
	{ month: 5, day: 16, name: "Ascension" },
	{ month: 5, day: 26, name: "Pentecôte" },
	{ month: 5, day: 27, name: "Lundi de Pentecôte" },
	{ month: 7, day: 14, name: "Fête Nationale" },
	{ month: 8, day: 15, name: "Assomption" },
	{ month: 11, day: 1, name: "Toussaint" },
	{ month: 11, day: 11, name: "Armistice" },
	{ month: 12, day: 25, name: "Noël" },
]

export const ZONE_A = [
	{ start: { month: 2, day: 6 }, end: { month: 2, day: 19 } },
	{ start: { month: 4, day: 8 }, end: { month: 4, day: 21 } },
	{ start: { month: 7, day: 8 }, end: { month: 9, day: 3}  },
	{ start: { month: 10, day: 15 }, end: { month: 10, day: 28 } },
	{ start: { month: 12, day: 18 }, end: { month: 12, day: 31 } },
]

export const ZONE_B = [
	{ start: { month: 2, day: 13 }, end: { month: 2, day: 26 } },
	{ start: { month: 4, day: 15 }, end: { month: 4, day: 28 } },
	{ start: { month: 7, day: 8 }, end: { month: 9, day: 3 } },
	{ start: { month: 10, day: 15 }, end: { month: 10, day: 28 } },
	{ start: { month: 12, day: 18 }, end: { month: 12, day: 31 } },
]

export const ZONE_C = [
	{ start: { month: 2, day: 20 }, end: { month: 3, day: 3 } },
	{ start: { month: 4, day: 22 }, end: { month: 5, day: 5 } },
	{ start: { month: 7, day: 8 }, end: { month: 9, day: 3 } },
	{ start: { month: 10, day: 15 }, end: { month: 10, day: 28 } },
	{ start: { month: 12, day: 18 }, end: { month: 12, day: 31 } },
]

export enum ActiveVacationZones {
	NONE = "Aucune",
	ALL = "Toutes",
	ZONE_A = "Zone A",
	ZONE_B = "Zone B",
	ZONE_C = "Zone C",
}

export enum VacationZones {
	ZONE_A = "A",
	ZONE_B = "B",
	ZONE_C = "C",
}

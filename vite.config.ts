import { fileURLToPath, URL } from "node:url"

import { defineConfig } from "vitest/config"
import vue from "@vitejs/plugin-vue"

// https://vitejs.dev/config/
export default defineConfig({
	build: {
		rollupOptions: {
			input: {
				index: "./index.html",
			},
			output: {
				manualChunks: {
					vue: ["vue", "vue-router"],
 					primevue: ["primevue", "@primevue/themes"],
				},
			},
		},
	},
	plugins: [vue()],
	resolve: {
		alias: [
			{
				find: "src",
				replacement: fileURLToPath(new URL("./src", import.meta.url)),
			},
		],
	},
	test: {
		coverage: {
			include: [
				"src/",
			],
			exclude: [
				"src/vite-env.d.ts",
			],
			thresholds: {
				statements: 0,
				branches: 20,
				functions: 10,
				lines: 0,
			},
		},
	},
})

FROM node:22-alpine AS builder
RUN apk add git
USER node
WORKDIR /home/node
COPY package.json package-lock.json ./
RUN npm ci
COPY ./ ./
RUN npm run build

FROM busybox:1-musl AS client
RUN adduser -D static
USER static
WORKDIR /home/static
COPY --from=builder /home/node/dist .
CMD ["busybox", "httpd", "-f", "-v", "-p", "8080"]

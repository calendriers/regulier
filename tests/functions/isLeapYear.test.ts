import { expect, it } from "vitest"

import isLeapYear from "src/functions/isLeapYear"

it("checks leap year", () => {
	const tests = [
        [1500, false],
        [1600, true],
        [1700, false],
        [1800, false],
        [1900, false],
		[1904, true],
		[1912, true],
		[1984, true],
		[1996, true],
		[2000, true],
		[2004, true],
		[2024, true],
	] as const

	for (const [year, expectedValue] of tests) {
		expect(isLeapYear(year)).toBe(expectedValue)
	}
})

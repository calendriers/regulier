import { expect, it } from "vitest"

import formatDateIsoString from "src/functions/formatDateIsoString"

it("formats date as ISO-8601 string", () => {
	const tests = [
		[new Date("1582-06-24"), "1582-06-24"],
		[new Date("1984-01-01"), "1984-01-01"],
		[new Date("2000-01-31"), "2000-01-31"],
		[new Date("2012-10-10"), "2012-10-10"],
		[new Date("2024-12-15"), "2024-12-15"],
	] as const

	for (const [date, expectedValue] of tests) {
		expect(formatDateIsoString(date)).toBe(expectedValue)
	}
})
